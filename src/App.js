import LoginPage from "./pages/auth/LoginPage";
import AdminDashboard from "./pages/dashboard/Admin";
import { Routes, Route } from "react-router-dom";

function App() {
  return (
    <div className="default-skin">
     <Routes>
        <Route path="/" element={<AdminDashboard />} />
        <Route path="login" element={<LoginPage />} />
      </Routes>
    </div>
  );
}

export default App;
