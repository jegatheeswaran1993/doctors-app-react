import React from 'react'
import PropTypes from 'prop-types';
import classNames from 'classnames';
import AuthHeader from '../components/common/auth/AuthHeader';
import AdminHeader from '../components/common/auth/AdminHeader';

export const Page = ({children, isAuth}) => {
    return (
        <main className={classNames({
            "auth": isAuth,
            "app": !isAuth
        })}>
            {isAuth?
            <AuthHeader /> : <AdminHeader />
            }
            {children}
        </main>
    )
}

Page.propTypes = {
	children: PropTypes.node,
	isAuth: PropTypes.bool,
};
Page.defaultProps = {
	children: null,
	isAuth: false,
};