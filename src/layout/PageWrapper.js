import React, { useLayoutEffect } from 'react'
import classNames from 'classnames';

const PageWrapper=({title, description,  className="wrapper", children})=> {
    useLayoutEffect(() => {
		document.getElementsByTagName('TITLE')[0].text = `${title ? `${title} | ` : ''}${
			process.env.REACT_APP_SITE_NAME
		}`;
		document
			.querySelector('meta[name="description"]')
			.setAttribute('content', description || process.env.REACT_APP_META_DESC);
	});
    return (
        <div className={classNames(className)}>
            {children}
        </div>
    )
}

export default PageWrapper
