import React from "react";
import PageWrapper from "../../layout/PageWrapper";
import { Page } from "../../layout/Page";
import "chart.js/auto";
import { Line } from "react-chartjs-2";

const data = {
  labels: ["January", "February", "March", "April", "May", "June", "July"],
  datasets: [
    {
      label: "My First dataset",
      fill: true,
      lineTension: 0.1,
      backgroundColor: "rgba(75,192,192,0.4)",
      borderColor: "rgba(75,192,192,1)",
      borderCapStyle: "butt",
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: "miter",
      pointBorderColor: "rgba(75,192,192,1)",
      pointBackgroundColor: "#fff",
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: "rgba(75,192,192,1)",
      pointHoverBorderColor: "rgba(220,220,220,1)",
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: [65, 59, 80, 81, 56, 55, 40],
    },
  ],
};

const AdminDashboard = () => {
  return (
    <PageWrapper title={"Dashboard"}>
      <Page className="app-main">
        <aside className="app-aside app-aside-expand-md app-aside-light">
          <div className="aside-content">
            <div className="aside-menu overflow-hidden ps">
              <nav
                id="stacked-menu"
                className="stacked-menu stacked-menu-has-collapsible"
              >
                <ul className="menu">
                  <li className="menu-item has-active">
                    <a href="index.html" className="menu-link">
                      <span className="menu-icon fas fa-home"></span>
                      <span className="menu-text">Dashboard</span>
                    </a>
                  </li>
                  <li className="menu-item">
                    <a href="index.html" className="menu-link">
                      <span className="menu-icon fas fa-home"></span>
                      <span className="menu-text">Hospital</span>
                    </a>
                  </li>
                  <li className="menu-item">
                    <a href="index.html" className="menu-link">
                      <span className="menu-icon fas fa-home"></span>
                      <span className="menu-text">Patient Details</span>
                    </a>
                  </li>
                </ul>
              </nav>
              <footer class="aside-footer border-top p-2">
                <button
                  class="btn btn-light btn-block text-primary"
                  data-toggle="skin"
                >
                  <span class="d-compact-menu-none">Version 1.0</span>{" "}
                  <i class="fas fa-moon ml-1"></i>
                </button>
              </footer>
            </div>
          </div>
        </aside>
        <div className="app-main">
          <div className="wrapper">
            <div className="page">
              <div className="page-inner">
                <div className="page-section">
                  <div className="row">
                    <div className="col-md-3">
                      <div className="card  dashboard">
                        <div className="card-body  p-4">
                          <div className="d-flex flex-wrap">
                            <span className="mr-4">
                              <i className="fa fa-heart text-danger" />
                            </span>
                            <div className="text-content">
                              <h6 className="card-title">Total Patient</h6>
                              <h3>783K</h3>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="card  dashboard">
                        <div className="card-body  p-4">
                          <div className="d-flex flex-wrap">
                            <span className="mr-4">
                              <i className="fa fa-hospital text-danger" />
                            </span>
                            <div className="text-content">
                              <h6 className="card-title">Hospital</h6>
                              <h3>783K</h3>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="card  dashboard">
                        <div className="card-body  p-4">
                          <div className="d-flex flex-wrap">
                            <span className="mr-4">
                              <i className="fas fa-clinic-medical text-danger" />
                            </span>
                            <div className="text-content">
                              <h6 className="card-title">Doctors</h6>
                              <h3>783K</h3>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="card  dashboard">
                        <div className="card-body  p-4">
                          <div className="d-flex flex-wrap">
                            <span className="mr-4">
                              <i className="fa fa-cart-plus text-danger" />
                            </span>
                            <div className="text-content">
                              <h6 className="card-title">Orders</h6>
                              <h3>783K</h3>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="card">
                    <div className="card-body">
                      <Line data={data} />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <div className="card">
                        <div className="card-header">
                          <h6>Top 5 Best Doctor</h6>
                        </div>
                        <div className="card-body">
                          <div className="mb-3">

                          
                          <div className="media align-items-center">
                            <div className="col-auto">
                              <div className="user-avatar user-avatar-xl">
                                <img src={"https://uselooper.com/assets/images/avatars/profile.jpg"} alt="user" />
                              </div>
                            </div>
                            <div className="col">
                              <div className="card-title text-truncate">
                              Dr. Samantha Queque
                              </div>
                              <div className="card-subtitle text-muted">Gynecologist</div>
                            </div>

                          </div>
                          </div>
                          <div className="mb-3">

                          
                          <div className="media align-items-center">
                            <div className="col-auto">
                              <div className="user-avatar user-avatar-xl">
                                <img src={"https://uselooper.com/assets/images/avatars/profile.jpg"} alt="user" />
                              </div>
                            </div>
                            <div className="col">
                              <div className="card-title text-truncate">
                              Dr. Samantha Queque
                              </div>
                              <div className="card-subtitle text-muted">Gynecologist</div>
                            </div>

                          </div>
                          </div>
                          <div className="mb-3">

                          
                          <div className="media align-items-center">
                            <div className="col-auto">
                              <div className="user-avatar user-avatar-xl">
                                <img src={"https://uselooper.com/assets/images/avatars/profile.jpg"} alt="user" />
                              </div>
                            </div>
                            <div className="col">
                              <div className="card-title text-truncate">
                              Dr. Samantha Queque
                              </div>
                              <div className="card-subtitle text-muted">Gynecologist</div>
                            </div>

                          </div>
                          </div>
                          <div className="mb-3">

                          
                          <div className="media align-items-center">
                            <div className="col-auto">
                              <div className="user-avatar user-avatar-xl">
                                <img src={"https://uselooper.com/assets/images/avatars/profile.jpg"} alt="user" />
                              </div>
                            </div>
                            <div className="col">
                              <div className="card-title text-truncate">
                              Dr. Samantha Queque
                              </div>
                              <div className="card-subtitle text-muted">Gynecologist</div>
                            </div>

                          </div>
                          </div>
                          
                          
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="card">
                        <div className="card-header">
                          <h6>Top 5 Best Doctor</h6>
                        </div>
                        <div className="card-body">
                          <div className="mb-3">

                          
                          <div className="media align-items-center">
                            <div className="col-auto">
                              <div className="user-avatar user-avatar-xl">
                                <img src={"https://uselooper.com/assets/images/avatars/profile.jpg"} alt="user" />
                              </div>
                            </div>
                            <div className="col">
                              <div className="card-title text-truncate">
                              Dr. Samantha Queque
                              </div>
                              <div className="card-subtitle text-muted">Gynecologist</div>
                            </div>

                          </div>
                          </div>
                          <div className="mb-3">

                          
                          <div className="media align-items-center">
                            <div className="col-auto">
                              <div className="user-avatar user-avatar-xl">
                                <img src={"https://uselooper.com/assets/images/avatars/profile.jpg"} alt="user" />
                              </div>
                            </div>
                            <div className="col">
                              <div className="card-title text-truncate">
                              Dr. Samantha Queque
                              </div>
                              <div className="card-subtitle text-muted">Gynecologist</div>
                            </div>

                          </div>
                          </div>
                          <div className="mb-3">

                          
                          <div className="media align-items-center">
                            <div className="col-auto">
                              <div className="user-avatar user-avatar-xl">
                                <img src={"https://uselooper.com/assets/images/avatars/profile.jpg"} alt="user" />
                              </div>
                            </div>
                            <div className="col">
                              <div className="card-title text-truncate">
                              Dr. Samantha Queque
                              </div>
                              <div className="card-subtitle text-muted">Gynecologist</div>
                            </div>

                          </div>
                          </div>
                          <div className="mb-3">

                          
                          <div className="media align-items-center">
                            <div className="col-auto">
                              <div className="user-avatar user-avatar-xl">
                                <img src={"https://uselooper.com/assets/images/avatars/profile.jpg"} alt="user" />
                              </div>
                            </div>
                            <div className="col">
                              <div className="card-title text-truncate">
                              Dr. Samantha Queque
                              </div>
                              <div className="card-subtitle text-muted">Gynecologist</div>
                            </div>

                          </div>
                          </div>
                          
                          
                        </div>
                      </div>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </Page>
    </PageWrapper>
  );
};

export default AdminDashboard;
