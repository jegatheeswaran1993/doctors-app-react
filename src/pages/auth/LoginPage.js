import React from "react";
import { Page } from "../../layout/Page";
import PageWrapper from "../../layout/PageWrapper";

const LoginPage = () => {
  return (
    <PageWrapper title={"Login"}>
      <Page isAuth={true}>
        <form className="auth-form">
          <div className="form-group">
            <div className="form-label-group">
              <input
                type="text"
                id="inputUser"
                className="form-control placeholder-shown"
                placeholder="Username"
              />
              <label htmlFor="inputUser">Username</label>
            </div>
          </div>
          <div className="form-group">
            <div className="form-label-group">
              <input
                type="text"
                id="password"
                className="form-control placeholder-shown"
                placeholder="Password"
              />
              <label htmlFor="password">Password</label>
            </div>
          </div>

          <div className="form-group">
            <button className="btn btn-lg btn-primary btn-block" type="submit">
              Sign In
            </button>
          </div>
          <div className="text-center pt-3">
            <a href="auth-recovery-username.html" className="link">
              Forgot Username?
            </a>{" "}
            <span className="mx-2">·</span>{" "}
            <a href="auth-recovery-password.html" className="link">
              Forgot Password?
            </a>
          </div>
        </form>
      </Page>
    </PageWrapper>
  );
};

export default LoginPage;
