import React from 'react'
import logo from "../../../assets/images/logo.png";
import header from "../../../assets/images/login-img-1.png";

const AuthHeader = () => {
    return (
        <header id="auth-header" className="auth-header" style={{backgroundImage:`url(${header})` }}>
        <h1>
          <img src={logo} alt="logo"/>
          <span className="sr-only">Sign In</span>
        </h1>
          <p> Don't have a account</p>
      </header>
    )
}

export default AuthHeader
