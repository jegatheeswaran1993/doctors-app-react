import React from 'react'
import logo from '../../../assets/images/logo.png'

const AdminHeader = () => {
    return (
        <header className="app-header app-header-light">
        <div className="top-bar">
          <div className="top-bar-brand">
            <button className="hamburger hamburger-squeeze mr-2" type="button">
              <span className="hamburger-box">
                <span className="hamburger-inner"></span>
              </span>
            </button>
              <img src={logo} alt='company logo' />
          </div>

          <div className="top-bar-list">
            <div className="top-bar-item top-bar-item-full">
              <div className="page-title">
                <span>Dashboard</span>
              </div>
              <div className="top-bar-item top-bar-item-right px-0 d-none d-sm-flex">
                <div className="dropdown d-none d-md-flex">
                  <button className="btn-account" type="button">
                    <span className="user-avatar user-avatar-md">
                      <img
                        src="https://uselooper.com/assets/images/avatars/profile.jpg"
                        alt="user pic "
                      />
                    </span>
                    <span className="account-summary pr-lg-4 d-none d-lg-block">
                      <span className="account-name">Beni Arisandi</span>
                      <span className="account-description">Admin</span>
                    </span>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
    )
}

export default AdminHeader
